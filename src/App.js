
import React from "react";
import PlayersList from "./PlayersList";

function App() {
  return (
    <div className="App">
      <header className="bg-gray-800 text-white text-center py-4">
        <h1 className="text-2xl font-bold">FIFA Players List</h1>
      </header>
      <main>
        <PlayersList />
      </main>
    </div>
  );
}

export default App;
