
import React from "react";

const Player = ({ name, team, nationality, jerseyNumber, age, imageUrl }) => {
  return (
    <div className="bg-white shadow-md rounded-lg overflow-hidden">
      <img src={imageUrl} alt={name} className="w-full h-48 object-cover" />
      <div className="p-4">
        <h2 className="text-xl font-bold mb-2">{name}</h2>
        <p className="text-sm text-gray-600 mb-2">Team: {team}</p>
        <p className="text-sm text-gray-600 mb-2">Nationality: {nationality}</p>
        <p className="text-sm text-gray-600 mb-2">Jersey Number: {jerseyNumber}</p>
        <p className="text-sm text-gray-600">Age: {age}</p>
      </div>
    </div>
  );
};

export default Player;
